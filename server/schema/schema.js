const graphql = require('graphql')
var _ = require('lodash')
const User = require('../model/user')
const Hobby = require('../model/hobby')
const Post = require('../model/post')

// //dummy data
// var usersData = [
//     {id: '1', name: 'Bond', age: 36, profession: 'Programmer'},
//     {id: '13', name: 'Anna', age: 26, profession: 'Baker'},
//     {id: '211', name: 'Bella', age: 16, profession: 'Mechanic'},
//     {id: '19', name: 'Gina', age: 26, profession: 'Painter'},
//     {id: '150', name: 'Georgina', age: 36, profession: 'Teacher'}
// ]

// var hobbiesData = [
//     {id: "1", title: 'Programming', description: 'Using computer to make the world better', userId: "1"},
//     {id: "2", title: 'Rowing', description: 'Sweet and feel better before eating donouts', userId: "1"},
//     {id: "3", title: 'Swimming', description: 'Get in the water and learn to become the water', userId: "211"},
//     {id: "4", title: 'Fencing', description: 'A hobby for fency people', userId: "150"},
//     {id: "5", title: 'Hiking', description: 'Wear hiking boots and explore the world', userId: "13"}
// ]

// var postsData = [
//     {id: "1", comment: "Building a mind", userId: "1"},
//     {id: "2", comment: "GraphQL is amazing", userId: "1"},
//     {id: "3", comment: "How to change the World", userId: "211"},
//     {id: "4", comment: "How to change the World", userId: "19"},
//     {id: "5", comment: "How to change the World", userId: "1"}
// ]

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull
} = graphql

// Create types
const UserType = new GraphQLObjectType({
    name: 'User',
    description: 'Documentation for user...',
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        age: {type: GraphQLInt},
        profession: {type: GraphQLString},
        posts: {
            type: GraphQLList(PostType),
            resolve(parent, args) {
                return Post.find({userId: parent.id})
                // return _.filter(postsData, {userId: parent.id})
            }
        },
        hobbies: {
            type: GraphQLList(HobbyType),
            resolve(parent, args) {
                return Hobby.find({userId: parent.id})
                // return _.filter(hobbiesData, {userId: parent.id})
            }
        }
    })
})

const HobbyType = new GraphQLObjectType({
    name: 'Hobby',
    description: 'Hobby description',
    fields: () => ({
        id: {type: GraphQLString},
        title: {type: GraphQLString},
        description: {type: GraphQLString},
        user: {
            type: UserType,
            resolve(parent, args) {
                return _.find(usersData, {id: parent.userId})
            }
        }
    })
})

const PostType = new GraphQLObjectType({
    name: 'Post',
    description: 'Post description',
    fields: () => ({
        id: {type: GraphQLString},
        comment: {type: GraphQLString},
        user: {
            type: UserType,
            resolve(parent, args) {
                return _.find(usersData, {id: parent.userId})
            }
        }
    })
})

// RootQuery
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    description: 'Description',
    fields: {
        user: {
            type: UserType,
            args: {id: {type: GraphQLString}},
            resolve(parent, args) {
                return _.find(usersData, {id: args.id})
                //we resolve with data
                //get and return data from a datasource
            }
        },

        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) {
                return usersData
            }
        },

        hobby: {
            type: HobbyType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return _.find(hobbiesData, {id: args.id})
            }
        },

        hobbies: {
            type: new GraphQLList(HobbyType),
            resolve(parent, args) {
                return hobbiesData
            }
        },

        post: {
            type: PostType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return _.find(postsData, {id: args.id})
            }
        },

        posts: {
            type: new GraphQLList(PostType),
            resolve(parent, args) {
                return postsData
            }
        }
    }
});

//Mutation 
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createUser: {
            type: UserType,
            args: {
                // id: {type: GraphQLID}
                name: {type: GraphQLNonNull(GraphQLString)},
                age: {type: GraphQLNonNull(GraphQLInt)},
                profession: {type: GraphQLString}
            },
            resolve(parent, args) {
                let user = User({
                    name: args.name,
                    age: args.age,
                    profession: args.profession
                })
                return user.save()
            }
        },

        //Update User
        UpdateUser : {
            type: UserType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)},
                name: {type: GraphQLNonNull(GraphQLString)},
                age: {type: GraphQLNonNull(GraphQLInt)},
                profession: {type: GraphQLString}
            },
            resolve(parent, args) {
                return UpdateUser = User.findByIdAndUpdate(
                    args.id,
                    {
                        $set: { 
                            name: args.name,
                            age: args.age,
                            profession: args.profession
                        }
                    },
                    {new: true}
                )
            }
        },

        //RemoveUser
        removeUser: {
            type: UserType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let removeUser = User.findByIdAndRemove(args.id).exec()
                if(!removeUser) {
                    throw new "Error"()
                }
                return removeUser
            }
        },

        createPost: {
            type: PostType,
            args: {
                userId: {type: GraphQLNonNull(GraphQLString)},
                comment: {type: GraphQLNonNull(GraphQLString)},
            },
            resolve(parent, args) {
                let post = Post({
                    userId: args.userId,
                    comment: args.comment
                })
                return post.save()
            }
        },

        //Update Post
        UpdatePost: {
            type: PostType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)},
                comment: {type: GraphQLNonNull(GraphQLString)},
            },
            resolve(parent, args) {
                return UpdatePost.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            comment: args.comment
                        }
                    },
                    {new: true}
                    )
            }
        },

        //Remove Post
        removePost: {
            type: PostType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let removePost = Post.findByIdAndRemove(args.id).exec()
                if(!removePost) {
                    throw new "Error"()
                }
                return removePost
            }
        },

        createHobby: {
            type: HobbyType,
            args: {
                userId: {type: GraphQLNonNull(GraphQLString)},
                title: {type: GraphQLNonNull(GraphQLString)},
                description: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let hobby = Hobby({
                    userId: args.userId,
                    title: args.title,
                    description: args.description
                })
                return hobby.save()
            }
        },

        //Update Hobby
        UpdateHobby: {
            type: HobbyType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)},
                title: {type: GraphQLNonNull(GraphQLString)},
                description: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                return UpdateHobby.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            title: args.title,
                            description: args.description
                        }
                    },
                    {new: true}
                )
            }
        },

        //Remove Post
        removeHobby: {
            type: HobbyType,
            args: {
                id: {type: GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                let removeHobby = Post.findByIdAndRemove(args.id).exec()
                if(!removeHobby) {
                    throw new "Error"()
                }
                return removeHobby
            }
        },
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})